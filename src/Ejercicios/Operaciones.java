package Ejercicios;

import java.util.ArrayList;
import java.util.List;

public class Operaciones {

	private static String TYPE = "01";

	public static void main(String[] args) {

		List<String> identifier = new ArrayList<String>();

		identifier.add("F001-11");
		identifier.add("F001-12");
		identifier.add("F001-13");
		identifier.add("F001-14");

		for (int i = 0; i < identifier.size(); i++) {

			
			String info = identifier.get(i);

			
			// TYPE 01 = facturas
			if (TYPE.equals("01")) {

				// Separando serie y correlativo
				String[] ids = info.split("-");

				System.out.println("Serie: "  + ids[0]);
				System.out.println("Correlativo: " + ids[1]);

			}

		}
	}

//	Adcionales
	// break; romper sentencia
	// metodos boolean retornar verdad o false

}
